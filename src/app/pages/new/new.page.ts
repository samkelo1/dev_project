import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-new',
  templateUrl: './new.page.html',
  styleUrls: ['./new.page.scss'],
})
export class NewPage implements OnInit {
  customer: any ={}; 
  constructor(
    private alertCtrl: AlertController,

  ) { }

  ngOnInit() {
  }

  customerForm = new FormGroup({
    first_name: new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.pattern("^[A-Za-z-]*$")])),
    last_name: new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.pattern("^[A-Za-z-]*$")])),
    mobile: new FormControl('', Validators.compose([Validators.pattern("^0(6|7|8){1}[0-9]{1}[0-9]{7}$"), Validators.required])),

  });



  validation_messages = {
    'first_name': [
      { type: 'required', message: 'First Name is required!' },
      { type: 'pattern', message: 'First Name should contain letters and hyphen only!' },
      { type: 'minlength', message: 'First Name should contain atleast 2 letters!' },
    ],
    'last_name': [
      { type: 'required', message: 'Last Name is required!' },
      { type: 'pattern', message: 'Last Name should contain letters and hyphen only!' },
      { type: 'minlength', message: 'Last Name should contain atleast 2 letters!' },
    ],
    'mobile': [
      { type: 'required', message: 'Mobile is required!' },
      { type: 'pattern', message: 'Enter South African Mobile Number' },
    ],
  

  };

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
 
  save() {
    if (this.customerForm.valid) {
      this.alertCtrl.create({
        message: 
        '<p>First Name:' + this.customer.first_name +'</p>'+
        '<p>Last Name :' + this.customer.last_name +'</p>'+
        '<p>Mobile    :' + this.customer.mobile +'</p>'
        ,
        cssClass: 'PopupAlert',
        mode: 'ios',
        buttons: [
          {
            text: 'Ok',
            cssClass: 'alertButton'
          }
        ]
      }).then(res => res.present());
    } else {
      this.validateAllFormFields(this.customerForm);
    }

  }

}
