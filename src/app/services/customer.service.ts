import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  private headers = new HttpHeaders();
  constructor(
    private http : HttpClient
  ) { }
  getCustomerData()  {
    return this.http.get('./assets/customer.json', { headers: this.headers });
  }
}
