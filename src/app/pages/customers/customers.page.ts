import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.page.html',
  styleUrls: ['./customers.page.scss'],
})
export class CustomersPage implements OnInit {
  customers : any [];
  constructor(
    private CustomerService: CustomerService
  ) { 

    this.CustomerService.getCustomerData().subscribe((data: any)=>{
      console.log(data)
      this.customers=data
    })
  }

  ngOnInit() {
  }

}
